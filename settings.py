# ROOTPATH = '/Users/shendazhong/Desktop/NIPS2021/experiments_v2/'
from os import path as op
ROOTPATH = op.dirname(op.abspath(__file__))
DATAPATH = op.join(ROOTPATH, 'data')
MODELPATH = op.join(ROOTPATH, 'models')
EN_STOP_WORDS = op.join(DATAPATH, 'EN_gensim_stopword.txt')

NEWS20_ADDR = op.join(DATAPATH ,'20news')
BNC_ADDR = op.join(DATAPATH, 'bnc')
TMN_ADDR = op.join(DATAPATH , 'tmn3')
Reuters_ADDR = op.join(DATAPATH, 'reuters')

GLOVE_ADDR = op.join(DATAPATH, 'word2vec')
EOS_UNK_PAD_EMD = op.join(DATAPATH, 'word2vec', 'EOS_UNK_PAD_300d.dct.npy')


LABELED_DATASETS=['News20','TMN']